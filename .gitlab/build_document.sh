#!/bin/sh

set -e
echo "Building document with latexmk..."
latexmk -cd -pdf -pdflatex="pdflatex -shell-escape %O %S" ./src/zauberwuerfel.tex
mkdir public
mv src/zauberwuerfel.pdf public
