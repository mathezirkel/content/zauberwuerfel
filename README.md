# Anleitung für Zauberwürfel (Rubik's Cube)

Dieses Repository enthält einen Merkzettel zum Lösen eines 3x3-Zauberwürfels.

## Dokument / Download

Die [aktuelle Version](https://mathezirkel.gitlab.io/content/zauberwuerfel/zauberwuerfel.pdf) der Anleitung wird automatisch aus diesem Repository kompiliert.
